minetest.register_node("colors:48280d", {
	description = "Dark Brown (#48280d)",
	tiles = {"colors_48280d.png"}
})

minetest.register_node("colors:5a381b", {
	description = "Light Brown (#5a381b)",
	tiles = {"colors_5a381b.png"}
})
minetest.register_node("colors:483712", {
	description = "Brown (#483712)",
	tiles = {"colors_483712.png"}
})
minetest.register_node("colors:796a44", {
	description = "Dark Tan (#796a44)",
	tiles = {"colors_796a44.png"}
})
minetest.register_node("colors:5f4617", {
	description = "Lightish Brown (#5f4617)",
	tiles = {"colors_5f4617.png"}
})
minetest.register_node("colors:e0d968", {
	description = "Peach (#e0d968)",
	tiles = {"colors_e0d968.png"}
})
minetest.register_node("colors:a6a549", {
	description = "Dark Peach (#a6a549)",
	tiles = {"colors_a6a549.png"}
})
