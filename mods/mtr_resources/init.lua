minetest.register_craftitem("mtr_resources:dirt_pieces", {
    description = "Dirt Pieces",
    inventory_image = "mtr_resources_dirt_pieces.png",
    stack_max = 64,
})
minetest.register_craftitem("mtr_resources:stick", {
    description = "Stick",
    inventory_image = "mtr_resources_stick.png",
    stack_max = 64,
})
