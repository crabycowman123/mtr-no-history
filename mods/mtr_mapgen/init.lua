local modpath = minetest.get_modpath("mtr_mapgen")

minetest.register_on_generated(
	function(minp, maxp, blockseed)
		if (minp.x < -23 and maxp.x > -23 and minp.y < -5 and maxp.y > -5 and minp.z < 8 and maxp.z > 8) then
			minetest.place_schematic({x = -22, y = -5, z = -18}, modpath.."/schematics/MTR_START2.mts", 0)
			minetest.set_node({x=-3, y=0, z=4}, {name="mtr_shop:shopkeeper"})
		end
	end
)
