local get_formspec = function(filename)
	local modpath = minetest.get_modpath("mtr_shop")
	local f = io.open(modpath.."/"..filename, "rb")
	local formspec = f:read("*all")
	f:close()
	return formspec
end 

minetest.register_node("mtr_shop:shopkeeper", {
	description = "Matthew",
	drawtype = "mesh",
	mesh = "shopkeeper.obj",
	tiles = { {name = "colors_a6a549.png"}, {name = "colors_e0d968.png"}, {name = "colors_796a44.png"}, {name = "colors_5f4617.png"}, {name = "colors_48280d.png"}, {name = "colors_483712.png"} },
	visual_scale = (1.0/16.0),
	selection_box = {
        type = "fixed",
        fixed = {
            {-6 / 16, -6 / 16, -6 / 16, 6 / 16, 22 / 16, 6 / 16},
        },
    },
    on_rightclick = function(pos, node, clicker, itemstack, pointed_thing)
    	minetest.show_formspec(clicker:get_player_name(), "mtr_shop:opening_shop", get_formspec("opening_shop.spec"))
    end,
    groups = {plastic = 5},
})

-- main shop formspec
minetest.register_on_player_receive_fields(function(player, formname, fields)
    if formname ~= "mtr_shop:opening_shop" then
        return
    end

    if fields.exit then
        minetest.show_formspec(player:get_player_name(), "mtr_shop:exit", "") -- TODO: figure out how to close formspecs programatically
    end
    
    if fields.shop then
    	minetest.show_formspec(player:get_player_name(), "mtr_shop:buy_sell_shop", get_formspec("buy_sell_shop.spec"))
    end
    
    -- TODO: warn or stop players who try to use the anvil without any items that can be transformed at the anvil
    if fields.anvil  and mtr_currency.subtract_credits(10, player:get_player_name()) then
    	minetest.show_formspec(player:get_player_name(), "mtr_shop:anvil", get_formspec("anvil.spec"))
    end
end)

minetest.register_on_player_receive_fields(function(player, formname, fields)
    if formname ~= "mtr_shop:buy_sell_shop" then
        return
    end

    if fields.back then
        minetest.show_formspec(player:get_player_name(), "mtr_shop:opening_shop", get_formspec("opening_shop.spec"))
    end
    
    local playerInvRef = minetest.get_inventory({type="player", name=player:get_player_name()})
    
    -- buy dirt
	if fields.bdirt then
		if (playerInvRef:room_for_item("main", ItemStack("mtr_resources:dirt_pieces")) and mtr_currency.subtract_credits(3, player:get_player_name())) then
			playerInvRef:add_item("main", ItemStack("mtr_resources:dirt_pieces"))
		end
	end
	
	-- sell dirt
	if fields.sdirt then
		if (playerInvRef:contains_item("main", ItemStack("mtr_resources:dirt_pieces")) and playerInvRef:remove_item("main", ItemStack("mtr_resources:dirt_pieces"))) then
			mtr_currency.add_credits(1, player:get_player_name())
		end
	end
	
	-- buy pebble
	if fields.bpebble then
		if (playerInvRef:room_for_item("main", ItemStack("mtr_weapons:pebble")) and mtr_currency.subtract_credits(10, player:get_player_name())) then
			playerInvRef:add_item("main", ItemStack("mtr_weapons:pebble"))
		end
	end
	
	-- sell pebble
	if fields.spebble then
		if (playerInvRef:contains_item("main", ItemStack("mtr_weapons:pebble")) and playerInvRef:remove_item("main", ItemStack("mtr_weapons:pebble"))) then
			mtr_currency.add_credits(5, player:get_player_name())
		end
	end
	
	-- buy stick
	if fields.bstick then
		if (playerInvRef:room_for_item("main", ItemStack("mtr_resources:stick")) and mtr_currency.subtract_credits(5, player:get_player_name())) then
			playerInvRef:add_item("main", ItemStack("mtr_resources:stick"))
		end
	end
	
	-- sell stick
	if fields.sstick then
		if (playerInvRef:contains_item("main", ItemStack("mtr_resources:stick")) and playerInvRef:remove_item("main", ItemStack("mtr_resources:stick"))) then
			mtr_currency.add_credits(3, player:get_player_name())
		end
	end
	
end)

minetest.register_on_player_receive_fields(function(player, formname, fields)
    if formname ~= "mtr_shop:anvil" then
        return
    end

    if fields.back then
        minetest.show_formspec(player:get_player_name(), "mtr_shop:opening_shop", get_formspec("opening_shop.spec"))
    end
    
    local playerInvRef = minetest.get_inventory({type="player", name=player:get_player_name()})
    
    -- make rock
	if fields.form then
		if (playerInvRef:contains_item("main", ItemStack("mtr_weapons:pebble 5")) and playerInvRef:room_for_item("main", ItemStack("mtr_weapons:rock")) and playerInvRef:remove_item("main", ItemStack("mtr_weapons:pebble 5"))) then
			playerInvRef:add_item("main", ItemStack("mtr_weapons:rock"))
		end
	end
	
	-- sharpen stick
	if fields.sharpen then
		if (playerInvRef:contains_item("main", ItemStack("mtr_resources:stick")) and playerInvRef:room_for_item("main", ItemStack("mtr_weapons:sharp_stick")) and playerInvRef:remove_item("main", ItemStack("mtr_resources:stick"))) then
			playerInvRef:add_item("main", ItemStack("mtr_weapons:sharp_stick"))
		end
	end
	
end)
