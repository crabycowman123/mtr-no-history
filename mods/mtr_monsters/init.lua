mobs:register_mob("mtr_monsters:dirt_monster", {
	type = "monster",
	hp_min = 1,
	hp_max = 2,
	damage = 0,
	visual = "mesh",
	textures = {"colors_48280d.png", "colors_5a381b.png" },
	mesh = "monsters_dirt_monster_revised.obj",
	walk_velocity = 1,
	run_velocity = 2,
	view_range = 10,
	visual_size = {x = 10 / 16, y = 10 / 16, z = 10 / 16},
	collisionbox = {-8/16, 0/16, -8/16, 8/16, 23/16, 8/16},
	pathfinding = 1,
	sounds = {
		random = "dirt_monster_growl_trimmed_modified",
	},
	drops = {
		{name = "mtr_resources:dirt_pieces", chance = 1.11, min = 0, max = 1},
		{name = "mtr_weapons:pebble", chance = 20, min = 0, max = 1},
	},
	attack_type = "dogfight",
	reach = 2,
	damage = 1,
})

mobs:spawn({
	name = "mtr_monsters:dirt_monster",
	active_object_count = 5,
})

minetest.register_chatcommand("summon_test_mob", {
	params = "",
	description = "Summon test mob",
	privs = {debug=true},
	func = function(name, param)
		mobs:add_mob({x = -9, y = 11, z = -11}, {
			name = "mtr_monsters:dirt_monster",
			ignore_count = true -- ignores mob count per map area
		})
	end,
})

-- Are timers reset when changing node type? If so, monster spawning may not work on first server run, since the node will be air first and then replaced with something from the schematic.
minetest.register_globalstep(function(dtime)
	if (math.random(100) == 1) then
		mobs:add_mob({x = 3, y = 1, z = -7}, {
			name = "mtr_monsters:dirt_monster",
		})
	end
	if (math.random(100) == 1) then
		mobs:add_mob({x = -20, y = 1.5, z = -1}, {
			name = "mtr_monsters:dirt_monster",
		})
	end
end)
