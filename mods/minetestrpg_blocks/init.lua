minetest.register_node("minetestrpg_blocks:dirt_block", {
	description = "Dirt",
	tiles = {"minetestrpg_blocks_dirt1.png"},
    groups = {plastic = 5},
})
minetest.register_node("minetestrpg_blocks:stone_block", {
	description = "Stone",
	tiles = {"minetestrpg_blocks_stone1.png"},
    groups = {plastic = 5},
})
minetest.register_node("minetestrpg_blocks:bricks1_block", {
	description = "Bricks",
	tiles = {"minetestrpg_blocks_bricks1.png"},
    groups = {plastic = 5},
})
minetest.register_node("minetestrpg_blocks:grass_covered_dirt1_block", {
	description = "Grass Block",
	tiles = {"minetestrpg_blocks_grass1.png", "minetestrpg_blocks_dirt1.png", "minetestrpg_blocks_grass_side1.png"},
    groups = {plastic = 5},
})
minetest.register_node("minetestrpg_blocks:glass1", {
	description = "Glass",
	tiles = {"minetestrpg_blocks_glass1.png"},
    drawtype = "glasslike",
    sunlight_propagates = true,
    paramtype = "light",
     -- TODO: Understand paramtype+use_texture_alpha...
	use_texture_alpha = minetest.features.use_texture_alpha_string_modes and "blend" or true,
    groups = {plastic = 5},
})
minetest.register_node("minetestrpg_blocks:grass_block", {
	description = "Block of Grass",
	tiles = {"minetestrpg_blocks_grass1.png"},
    groups = {plastic = 5},
})
minetest.register_node("minetestrpg_blocks:wood", {
	description = "Wood",
	tiles = {"minetestrpg_blocks_wood_v.png", "minetestrpg_blocks_wood_v.png", "minetestrpg_blocks_wood_h.png"},
    groups = {plastic = 5},
})
minetest.register_node("minetestrpg_blocks:leaves1", {
	description = "Leaves 1",
	tiles = {"minetestrpg_blocks_leaves1.png"},
    drawtype = "allfaces",
    sunlight_propagates = true,
    paramtype = "light",
    groups = {plastic = 5},
})
minetest.register_node("minetestrpg_blocks:fake_sky", {
	description = "FakeSky",
	tiles = {"minetestrpg_blocks_fakesky.png"},
    sunlight_propagates = true,
    paramtype = "light",
    groups = {plastic = 5},
})
minetest.register_node("minetestrpg_blocks:2d_grass", {
	description = "2D Grass",
	tiles = {"minetestrpg_blocks_grass2d.png"},
    drawtype = "plantlike",
    sunlight_propagates = true,
    paramtype = "light",
    groups = {plastic = 5, dig_immediate = 3},
})

minetest.register_node("minetestrpg_blocks:lava", {
	description = "Lava",
	drawtype = "liquid",
	tiles = {
		{
			name = "minetestrpg_blocks_lava.png",
			animation = {
				type = "vertical_frames",
				aspect_w = 16,
				aspect_h = 16,
				length = 10.0,
			},
		}
	},
	paramtype = "light",
	light_source = 15,
	walkable = false,
	pointable = false,
	diggable = false,
	buildable_to = true,
	drowning = 1,
	liquidtype = "source",
	liquid_viscosity = 7,
	damage_per_second = 8,
	groups = {lava = 3, liquid = 2, plastic = 3},
})
minetest.register_node("minetestrpg_blocks:dark_bricks", {
	description = "Dark Bricks",
	tiles = {"minetestrpg_blocks_dark_bricks.png"},
    groups = {plastic = 5},
})
minetest.register_node("minetestrpg_blocks:lava_dark_bricks", {
	description = "Lava Covered Dark Bricks",
		tiles = {
		{
			name = "minetestrpg_blocks_lava_covered_dark_bricks.png",
			animation = {
				type = "vertical_frames",
				aspect_w = 16,
				aspect_h = 16,
				length = 10.0,
			},
		}
	},
	groups = {plastic = 5},
	damage_per_second = 2,
	light_source = 15,
	paramtype = "light",
})
