mobs:register_arrow("mtr_weapons:rock_entity", {
	visual = "sprite",
	textures = {"mtr_weapons_rock.png"},
	velocity = 6,

	hit_player = function(self, player)
		if (nil == self.playername) then
			return -- TODO: allow throwing projectiles downward
		end
		player:punch(minetest.get_player_by_name(self.playername) or self.object, 1.0, {
			damage_groups = {fleshy = 10},
		}, nil)
	end,

	hit_mob = function(self, player)
		player:punch(self.object, 1.0, {
			damage_groups = {fleshy = 10},
		}, nil)
	end,
	
	hit_node = function(self, player)
	
	end,
})

mobs:register_arrow("mtr_weapons:pebble_entity", {
	visual = "sprite",
	textures = {"mtr_weapons_pebble.png"},
	velocity = 6,

	hit_player = function(self, player)
		if (nil == self.playername) then
			return -- TODO: allow throwing projectiles downward
		end
		player:punch(minetest.get_player_by_name(self.playername) or self.object, 1.0, {
			damage_groups = {fleshy = 10},
		}, nil)
	end,

	hit_mob = function(self, player)
		player:punch(self.object, 1.0, {
			damage_groups = {fleshy = 1},
		}, nil)
	end,
	
	hit_node = function(self, player)
	
	end,
})

local GRAVITY = 9
local VELOCITY = 19

local throw = function (item, player, pointed_thing)

	local itemname = item:get_name()

	local playerpos = player:get_pos()

	local obj = minetest.add_entity({
		x = playerpos.x,
		y = playerpos.y +1.5,
		z = playerpos.z
	}, itemname.."_entity")

	local ent = obj:get_luaentity()
	local dir = player:get_look_dir()

	ent.velocity = VELOCITY -- needed for api internal timing
	ent.switch = 1 -- needed so that egg doesn't despawn straight away
	ent._is_arrow = true -- tell advanced mob protection this is an arrow

	obj:set_velocity({
		x = dir.x * VELOCITY,
		y = dir.y * VELOCITY,
		z = dir.z * VELOCITY,
	})

	obj:set_acceleration({
		x = dir.x * -3,
		y = -GRAVITY,
		z = dir.z * -3
	})

	item:take_item()

	return item
end

minetest.register_craftitem("mtr_weapons:rock", {
    description = "Grassland Rock",
    inventory_image = "mtr_weapons_rock.png",
    stack_max = 4,
    on_use = throw
})

minetest.register_craftitem("mtr_weapons:pebble", {
    description = "Pebble",
    inventory_image = "mtr_weapons_pebble.png",
    stack_max = 16,
    on_use = throw
})
minetest.register_tool("mtr_weapons:sharp_stick", {
    description = "Sharp Stick",
    inventory_image = "mtr_weapons_sharp_stick.png",
    stack_max = 1,
    tool_capabilities = {
        full_punch_interval = 0.25,
        snappy={times={[2]=1.6, [3]=0.40}, maxlevel=1},
        damage_groups = {fleshy=1},
    }
})
