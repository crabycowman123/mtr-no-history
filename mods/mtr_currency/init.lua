mtr_currency = {}
local currencyStorageRef = minetest.get_mod_storage();
local hud_ids = {}

mtr_currency.get_credits = function(playername)
	local data = minetest.deserialize(currencyStorageRef:get_string("credits")) or {}
	return data[playername]
end

mtr_currency.set_credits = function(credits, playername)
	local data = minetest.deserialize(currencyStorageRef:get_string("credits")) or {}
	print(data)
	data[playername] = credits
	currencyStorageRef:set_string("credits", minetest.serialize(data))
	if hud_ids[playername] then
		minetest.get_player_by_name(playername):hud_remove(hud_ids[playername])
	end
	hud_ids[playername] = minetest.get_player_by_name(playername):hud_add({
    	hud_elem_type = "text",
    	position = {x = 0.95, y = 0.1},
    	scale = {x = 2, y = 2},
    	name = "coin_amount",
    	text = mtr_currency.get_credits(playername),
    	z_index = -99,
    })
end

mtr_currency.add_credits = function(credits, playername)
	if (credits < 0) then
		return false, "You should use subtract_credits instead"
	end
	mtr_currency.set_credits(mtr_currency.get_credits(playername) + credits, playername)
	return true
end

mtr_currency.subtract_credits = function(credits, playername)
	if (credits < 0) then
		return false, "You should use add_credits instead"
	end
	if (credits > mtr_currency.get_credits(playername)) then
		return false, "You don't have enough credits"
	end
	mtr_currency.set_credits(mtr_currency.get_credits(playername) - credits, playername)
	return true
end

minetest.register_chatcommand("reset", {
	params = "",
	description = "Reset credit amount",
	privs = {give=true},
	func = function(name, param)
		print("Erasing "..currencyStorageRef:get_float("credits").." credits...")
		currencyStorageRef:set_float("credits", 0.0)
	end,
})

minetest.register_chatcommand("get_credits", {
	params = "",
	description = "Print credit amount",
	prive = {interact=true},
	func = function(name, param)
		minetest.chat_send_player(name, "Credits: "..mtr_currency.get_credits(name))	
	end,
})

minetest.register_chatcommand("add_credits", {
	params = "<credits>",
	description = "Add credits",
	prive = {gie=true},
	func = function(name, param)
		if not (tonumber(param)) then
			return false, "Parameter must be a number"
		end
		return mtr_currency.add_credits(tonumber(param), name)
	end,
})

minetest.register_chatcommand("subtract_credits", {
	params = "<credits>",
	description = "Subtract credits",
	prive = {give=true},
	func = function(name, param)
		if not (tonumber(param)) then
			return false, "Parameter must be a number"
		end
		return mtr_currency.subtract_credits(tonumber(param), name)
	end,
})

minetest.register_chatcommand("set_credits", {
	params = "<credits>",
	description = "Set credits",
	prive = {give=true},
	func = function(name, param)
		if not (tonumber(param)) then
			return false, "Parameter must be a number"
		end
		return mtr_currency.set_credits(tonumber(param), name)
	end,
})

minetest.register_on_joinplayer(function(player, laston)
    local playername = player:get_player_name()
    local data = minetest.deserialize(currencyStorageRef:get_string("credits")) or {}
    if data == nil or data[playername] == nil then
        mtr_currency.set_credits(0, playername)
    end
    
    player:hud_add({
    	hud_elem_type = "image",
    	position = {x = 0.93, y = 0.1},
    	scale = {x = 2, y = 2},
    	name = "coin_icon",
    	text = "gold_coin.png",
    	z_index = -100,
    })
    
    hud_ids[player:get_player_name()] = player:hud_add({
    	hud_elem_type = "text",
    	position = {x = 0.95, y = 0.1},
    	scale = {x = 2, y = 2},
    	name = "coin_amount",
    	text = mtr_currency.get_credits(player:get_player_name()),
    	z_index = -99,
    })
end)
