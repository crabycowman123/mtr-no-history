minetest.register_tool("mtr_debug:pure_imagine_pick", {
	description = "pickaxe",
	inventory_image = "mtr_debug_pure_imagine_pick.png",
	tool_capabilities = {
		groupcaps={
			plastic = {maxlevel=12496, times = {42, 42, 42, 42, 42}},
		},
		damage_groups = {fleshy=12496},
	},
	groups = {pickaxe = 1}
})
